# Application de gestion de frigo

- Cette application Vue a pour but de réaliser la gestion des produits d'un frigo.
- L'accès au frigo est réalisé depuis une API, hébergée à l'adresse "https://webmmi.iut-tlse3.fr/~pecatte/frigo/public/6/".
- Réalisée par Kevin GAREL dans le cadre du module sur les technologies Web, enseigné en FIA3 à l'école d'ingénieurs ISIS.

# 🖥️ Fonctionnalités
## 🌐 Menu de navigation
La barre de navigation, située en haut de l'écran, permet à l'utilisateur de naviguer entre les différentes pages de l'application :
- **FRIGO** : Page principale de l'application
- **AJOUTER UN PRODUIT** : Formulaire permettant d'ajouter un produit au frigo.
- **A PROPOS** : Page d'informations sur l'application.

## 🏚️ Page d'accueil
Chemin d'accès : `/`
- La page d'accueil de l'application affiche un frigo fermé.
- Lorsque l'utilisateur clique sur la poignée de la porte, il est redirigé vers la page de gestion des produits du frigo.

## 📦 Page de gestion des produits
Chemin d'accès : `/PageFridge`
- Cette page affiche le frigo ouvert. Elle est composée de deux parties :
  - **Liste des produits** : Affichage et gestion des produits actuellement présents dans le frigo.
  - **Porte du frigo** : Un timer est affiché sur la porte du frigo. Un clic sur la porte permet de refermer le frigo et de retourner sur la page d'accueil.

### 📦 Affichage des produits
- Les produits sont affichés sous forme de cartes Vuetify.
- Chaque carte contient les informations suivantes :
  - **Photo** : Photo du produit (si renseignée)
  - **Nom du produit** : Nom du produit
  - **Quantité** : Quantité du produit
  - **Boutons** : 
    - **Bouton ➖** : permet de retirer 1 à la quantité du produit.
    - **Bouton ➕** : permet d'ajouter 1 à la quantité du produit.
    - **Bouton 🗑️** : permet de supprimer le produit du frigo.
- Si aucun produit n'est présent dans le frigo, un message s'affiche à la place des cartes.

### 🔍 Recherche de produit
- Un champ de recherche permet de filtrer les produits affichés.
- La recherche s'effectue sur le nom des produits.
- La recherche est insensible à la casse.
- La recherche est dynamique, les produits sont filtrés à chaque saisie de caractère.
- Un bouton ❌ permet de réinitialiser la recherche et d'afficher tous les produits.

### ⏲️ Timer d'ouverture de porte
- Dès l'ouverture de la porte du frigo, un timer de 2 minutes est lancé.
- L'utilisateur doit fermer la porte du frigo avant la fin du timer.
- Lorsque le timer arrive à 0, les produits sont périmés.
- Les images des produits périmés sont affichées en négatif.
- Une alerte apparaît signalant à l'utilisateur que les produits sont périmés et que le frigo doit être vidé.
- Lors de l'appui sur le bouton de l'alerte, ou en fermant la page, tous les produits sont supprimés du frigo.

*Conseil : tester cette fonctionnalité en dernier pour ne pas vider tous les produits actuellement présents dans le frigo.*

## 📝 Formulaire d'ajout de produit
Chemin d'accès : `/PageAddItem`
- Ce formulaire permet d'ajouter un produit au frigo. Trois champs sont disponibles :
  - **Nom du produit** : Champ de saisie du nom du produit (REQUIS)
  - **Quantité** : Champ de saisie de la quantité du produit (REQUIS)
  - **Photo** : Champ de saisie du lien vers la photo du produit (OPTIONNEL)
- Un bouton "AJOUTER LE PRODUIT" permet d'envoyer le formulaire.
- Un message d'erreur s'affiche si le formulaire n'est pas correctement rempli.
- Un message de succès s'affiche si le produit a été ajouté correctement.
### 🔠 Champ de saisie du nom du produit
- Le nom du produit peut être n'importe quelle chaîne de caractères.
- Si le champ n'est pas renseigné, le formulaire n'est pas envoyé, et un message d'erreur s'affiche.

### 🔢 Champ de saisie de la quantité du produit
- Les boutons ➕ et ➖ permettent d'incrémenter ou de décrémenter la valeur du champ.
- On peut également saisir un nombre entier compris entre 1 et 50.
- Si la valeur saisie est supérieure à 50, elle reste bloquée à 50.
- Si la valeur saisie est inférieure à 1, elle reste bloquée à 1.
- Si la valeur n'est pas renseignée, le formulaire n'est pas envoyé, et un message d'erreur s'affiche.

### 📷 Champ de saisie de la photo du produit
- Le champ de saisie de la photo du produit est optionnel.
- Si le champ n'est pas renseigné, le formulaire est envoyé, et le produit est ajouté sans photo.
- Si le champ est renseigné, il doit contenir un lien valide vers une image.
- Le lien doit obligatoirement commencer par `http://` ou `https://`.
- Le lien ne doit pas contenir d'espace.
- Le lien doit obligatoirement terminer par une extension d'image parmi les suivantes : `.jpg`, `.jpeg`, `.png`, `.gif`, `.webp`.
- Si le lien n'est pas valide, le formulaire n'est pas envoyé, et un message d'erreur s'affiche.

## 📜 Page "A Propos"
Chemin d'accès : `/PageAbout`
- La page "A propos" récapitule les informations contenues dans ce document.
