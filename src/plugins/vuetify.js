/**
 * plugins/vuetify.js
 *
 * Framework documentation: https://vuetifyjs.com`
 */

// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Composables
import { createVuetify } from 'vuetify'
import { VNumberInput } from 'vuetify/labs/VNumberInput'


// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
export default createVuetify({
  theme: {
    defaultTheme: 'dark',
    themes: {
      dark: {
        dark: true,
        colors:{
          primary: "#690B0B",
          secondary: "#a80505",
          buttons: "#eb463b",
          altButtons: "#f08400",
          error: "#F44336",
          warning: "#FFEB3B",
          info: "#f08400",
          success: "#4CAF50",
        }
      }
    }
  },
  components: {
    VNumberInput,
  },
})
