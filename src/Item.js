export class Item {
  constructor (itemJSON) { // en paramètre un item au format JSON
    this._id = itemJSON.id
    this._name = itemJSON.nom
    this._qty = itemJSON.qte
    this._picture = itemJSON.photo ?? ""
  }
  get id() { return this._id }
  get name() { return this._name }
  get qty() { return this._qty }
  get picture() { return this._picture }

  toString() { return `--> ${this._name} (${ this._qty})`; }
}
